#include <Arduino.h>
#include <Wire.h>
#include "TFMini.h"
#include "DHT.h"
#define DHTPIN 14
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
const int triggerPin = 2; // Trigger Pin of Ultrasonic Sensor
const int echoPin = 4;    // Echo Pin of Ultrasonic Sensor
long duracion;
const int MPU_addr=0x68;  // I2C address of the MPU-6050
TFMini tfmini;
//https://learn.sparkfun.com/tutorials/tfmini---micro-lidar-module-hookup-guide

float getTemperature(int);
float getHumidity(int);
float getDistanceUltrasonic(int);
uint16_t getDistanceLidar(int);
void getAccelerometerMeasure();
long timeInterval = 500; // interval time to read all sensors
void setup()
{
  Wire.begin();
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x6B);  // PWR_MGMT_1 register
  Wire.write(0);     // set to zero (wakes up the MPU-6050)
  Wire.endTransmission(true);
  // Step 1: Initialize hardware serial port (serial debug port)
  Serial2.begin(115200);
  Serial.begin(115200);
  dht.begin();
  pinMode(triggerPin, OUTPUT);
  pinMode(echoPin, INPUT);
  // wait for serial port to connect. Needed for native USB port only
  while (!Serial)
    ;

  Serial.println("Initializing...");

  // Se pasa cómo parámetro el puerto serial al cual está conectado el sensor
  tfmini.begin(&Serial2);
  delay(100);
  tfmini.setSingleScanMode();
}

void loop()
{
  float temperature = 0, humidity=0, distanceUltrasonic=0;
  uint16_t distanceLidar = 0;
  temperature = getTemperature(10);
  humidity = getHumidity(10);
  distanceUltrasonic = getDistanceUltrasonic(10);
  distanceLidar = getDistanceLidar(10);
  // Take one TF Mini distance measurement
  Serial.print("Lidar : ");
  Serial.print(distanceLidar);
  Serial.println(" cm ");
  Serial.print("Ultrasonic: ");
  Serial.print(distanceUltrasonic);
  Serial.println(" cm ");
  Serial.print("Temperature: ");
  Serial.print(temperature);
  Serial.println(" °C ");
  Serial.print("Humidity: ");
  Serial.print(humidity);
  Serial.println(" % ");
  getAccelerometerMeasure();
  delay(500);
}

float getTemperature(int numMuestras)
{
  float temperature = 0;
  for (int i = 0; i < numMuestras; i++)
  {
    temperature = temperature + dht.readTemperature();
  }
  temperature = temperature / numMuestras;
  return temperature;
}

float getHumidity(int numMuestras)
{
  float humidity = 0;
  for (int i = 0; i < numMuestras; i++)
  {
    humidity = humidity + dht.readHumidity();
  }
  humidity = humidity / numMuestras;
  return humidity;
}

float getDistanceUltrasonic(int numMuestras)
{
  float ultrasonicDist = 0;
  long duration;
  for (int i = 0; i < numMuestras; i++)
  {
    digitalWrite(triggerPin, HIGH);
    delay(1);
    digitalWrite(triggerPin, LOW);
    duracion = pulseIn(echoPin, HIGH);
    float distance = duration / 58.2;
    distance = 1.0182 * distance - 0.4428;
    ultrasonicDist = ultrasonicDist + distance;
  }
  ultrasonicDist = ultrasonicDist / numMuestras;
  return ultrasonicDist;
}
uint16_t getDistanceLidar(int numMuestras){
  uint16_t averageDist = 0;

  for (int i = 0; i < numMuestras; i++)
  {
    tfmini.externalTrigger();
    //0,9968x - 3,604
    averageDist = averageDist + (0.9968 * tfmini.getDistance() - 3.604);
    // Display the measurement
    // Se debe esperar cómo mínimo 25ms para la próxima medida
    delay(25);
  }
  averageDist = averageDist / numMuestras;
}

void getAccelerometerMeasure(){
  int16_t AcX,AcY,AcZ,Tmp,GyX,GyY,GyZ;
  Wire.beginTransmission(MPU_addr);
  Wire.write(0x3B);  // starting with register 0x3B (ACCEL_XOUT_H)
  Wire.endTransmission(false);
  Wire.requestFrom(MPU_addr,14,true);  // request a total of 14 registers
  AcX=Wire.read()<<8|Wire.read();  // 0x3B (ACCEL_XOUT_H) & 0x3C (ACCEL_XOUT_L)    
  AcY=Wire.read()<<8|Wire.read();  // 0x3D (ACCEL_YOUT_H) & 0x3E (ACCEL_YOUT_L)
  AcZ=Wire.read()<<8|Wire.read();  // 0x3F (ACCEL_ZOUT_H) & 0x40 (ACCEL_ZOUT_L)
  Tmp=Wire.read()<<8|Wire.read();  // 0x41 (TEMP_OUT_H) & 0x42 (TEMP_OUT_L)
  GyX=Wire.read()<<8|Wire.read();  // 0x43 (GYRO_XOUT_H) & 0x44 (GYRO_XOUT_L)
  GyY=Wire.read()<<8|Wire.read();  // 0x45 (GYRO_YOUT_H) & 0x46 (GYRO_YOUT_L)
  GyZ=Wire.read()<<8|Wire.read();  // 0x47 (GYRO_ZOUT_H) & 0x48 (GYRO_ZOUT_L)
  Serial.print("AcX = "); Serial.print(AcX);
  Serial.print(" | AcY = "); Serial.print(AcY);
  Serial.print(" | AcZ = "); Serial.print(AcZ);
  Serial.print(" | Tmp = "); Serial.print(Tmp/340.00+36.53);  //equation for temperature in degrees C from datasheet
  Serial.print(" | GyX = "); Serial.print(GyX);
  Serial.print(" | GyY = "); Serial.print(GyY);
  Serial.print(" | GyZ = "); Serial.println(GyZ);
  delay(333);
}